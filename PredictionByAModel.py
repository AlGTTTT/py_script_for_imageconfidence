import tensorflow as tf
import numpy as np
from tensorflow.keras.preprocessing.image import img_to_array, load_img
from tensorflow.keras.models import Model, load_model

from tensorflow.keras.utils import to_categorical
import cv2

# image_path = 'data/train/benign'
image_path = 'data/validation/benign/AOI_1257_189102020_06_041_Symmetry_II.png'

# dimensions of images
img_width, img_height = 224, 224
# img_width, img_height = 368, 208

# loading the model
# model = load_model('Model_Spleen_4Conv_3Dense.h5')
model = load_model('Model_InceptV3_Spleen_Ben_Mal_SymII_1st_Iter.h5')

# loading the class label dictionary
class_dictionary = np.load('class_indices.npy', allow_pickle=True).item()

# loading and preprocessing the images
image_orig = load_img(image_path, target_size=(img_width, img_height), interpolation='lanczos')
image = img_to_array(image_orig)

image = image / 255.0

#adding a new axis
image = np.expand_dims(image, axis=0)

# getting the probability of the prediction
probabilities = model.predict(image)

# decoding the prediction
prediction_probability = probabilities[0, probabilities.argmax(axis=1)][0]

class_predicted = np.argmax(probabilities, axis=1)
inID = class_predicted[0]

# inverting the class dictionary to get the label for the id
inv_map = {v: k for k, v in class_dictionary.items()}
label = inv_map[inID]

print("[Info] Predicted: {}, Confidence: {:.5f}%".format(label,prediction_probability*100))

# using OpenCV to display the image with the additional information
image_cv = cv2.imread(image_path)
image_cv = cv2.resize(image_cv, (600, 600), interpolation=cv2.INTER_LINEAR)
cv2.putText(image_cv,
            "Predicted: {}".format(label),
            (20, 40), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)


cv2.putText(image_cv,
            "Confidence: {:.5f}%".format(prediction_probability*100),
            (20, 80), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

cv2.imshow("Prediction", image_cv)
cv2.waitKey(0)

cv2.destroyAllWindows()
# cv2.destroyWindow()








                   


    
